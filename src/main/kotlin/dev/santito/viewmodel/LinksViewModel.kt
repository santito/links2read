package dev.santito.viewmodel

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import dev.santito.database.Link
import dev.santito.database.LinkDatabase


class LinksViewModel(val database: LinkDatabase) {

    private val _links = mutableStateListOf<Link>()
    val links: SnapshotStateList<Link> = _links

    init {
        getAllLinks()
    }

    private fun getAllLinks() {
        _links.clear()
        _links.addAll(database.selectAll())
    }

    fun addLink(title: String, url: String) {
        database.insert(title, url)
        getAllLinks()
    }

    fun updateLink(title: String, url: String, id: Long) {
        database.update(title, url, id)
        getAllLinks()
    }

    fun deleteLink(id: Long) {
        database.delete(id)
        getAllLinks()
    }
}