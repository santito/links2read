package dev.santito.database

import com.squareup.sqldelight.db.SqlDriver

class LinkDatabaseImpl(driver: SqlDriver): LinkDatabase {

    private val queries = Database(driver).linkQueries

    override fun selectAll(): List<Link> = queries.selectAll().executeAsList()

    override fun insert(title: String, url: String) = queries.insert(title, url)

    override fun update(title: String, url: String, id: Long) = queries.update(title, url, id)

    override fun delete(id: Long) = queries.delete(id)
}