package dev.santito.database

interface LinkDatabase {

    fun selectAll(): List<Link>

    fun insert(title: String, url: String)

    fun update(title: String, url: String, id: Long)

    fun delete(id: Long)
}