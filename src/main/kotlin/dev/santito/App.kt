package dev.santito

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import com.squareup.sqldelight.sqlite.driver.JdbcSqliteDriver
import dev.santito.database.Database
import dev.santito.database.LinkDatabaseImpl
import dev.santito.ui.LinkScaffold
import dev.santito.viewmodel.LinksViewModel
import java.io.File


fun main() = application {
        Window(
            onCloseRequest = ::exitApplication,
            title = "Links2Read"
        ) {
            Surface(modifier = Modifier.fillMaxSize()) {
                val driver = JdbcSqliteDriver(url = "jdbc:sqlite:${File("build/database.db").canonicalFile.absolutePath}").apply {
                    Database.Schema.create(this)
                }
                val database = LinkDatabaseImpl(driver)
                val viewModel = LinksViewModel(database)
                LinkScaffold(viewModel)
            }
        }
}


