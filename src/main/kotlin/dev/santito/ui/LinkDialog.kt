package dev.santito.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import dev.santito.database.Link
import dev.santito.viewmodel.LinksViewModel
import java.net.URL

enum class Mode {
    ADD, EDIT
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterialApi::class)
@Composable
fun LinkDialog(
    viewModel: LinksViewModel,
    mode: Mode,
    link: Link?,
    onDismiss: () -> Unit
) {
    val (title, setTitle) = remember { mutableStateOf(link?.title ?: "") }
    val (url, setUrl) = remember { mutableStateOf(link?.url ?: "") }
    val (isUrlValid, setIsUrlValid) = remember { mutableStateOf(true) }

    AlertDialog(
        onDismissRequest = onDismiss,
        title = {
            Text( text = when(mode) {
                Mode.EDIT -> "Edit Link"
                Mode.ADD -> "Add Link"
            },
                style = MaterialTheme.typography.headlineSmall,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(bottom = 16.dp)
            )
        },
        confirmButton = {
            Button(
                onClick = {
                    if (isUrlValid) { // only add/update the link if the URL is valid
                        when(mode) {
                            Mode.EDIT -> viewModel.updateLink(title, url, link!!.id)
                            Mode.ADD -> viewModel.addLink(title, url)
                        }
                        onDismiss()
                    }
                }
            ) {
                Text(
                    when(mode) {
                        Mode.EDIT -> "Save"
                        Mode.ADD -> "Add"
                    }
                )
            }
        },
        dismissButton = {
            Button(onClick = onDismiss) {
                Text("Cancel")
            }
        },
        text = {
            Column {
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = title,
                    onValueChange = { setTitle(it) },
                    label = { Text("Title") }
                )

                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = url,
                    onValueChange = {
                        setUrl(it)
                        setIsUrlValid(try {
                            URL(it)
                            true
                        } catch (e: Exception) {
                            false
                        })
                    },
                    label = { Text("URL") },
                    isError = !isUrlValid
                )

                if (!isUrlValid) {
                    Text(
                        text = "Please enter a valid URL",
                        color = MaterialTheme.colorScheme.error,
                        style = MaterialTheme.typography.labelSmall,
                        modifier = Modifier.padding(top = 4.dp)
                    )
                }
            }
        }
    )
}