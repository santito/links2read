package dev.santito.ui

import LinkItem
import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.ContentAlpha
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import dev.santito.database.Link
import dev.santito.theme.DarkColors
import dev.santito.theme.LightColors
import dev.santito.viewmodel.LinksViewModel
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LinkScaffold(viewModel: LinksViewModel) {
    val colors = if (!isSystemInDarkTheme()) {
        LightColors
    } else {
        DarkColors
    }

    val (dialogMode, setDialogMode) = remember { mutableStateOf(Mode.ADD) }
    val (isDialogOpen, setIsDialogOpen) = remember { mutableStateOf(false) }
    val (linkToEdit, setLinkToEdit) = remember { mutableStateOf<Link?>(null) }

    MaterialTheme(
        colorScheme = colors,
        typography = MaterialTheme.typography.copy(
            headlineMedium = MaterialTheme.typography.headlineMedium.copy(fontWeight = FontWeight.Bold),
            headlineSmall = MaterialTheme.typography.headlineSmall.copy(
                fontWeight = FontWeight.Normal,
                color = colors.onBackground.copy(alpha = ContentAlpha.medium)
            ),
            bodyLarge = MaterialTheme.typography.bodyLarge.copy(
                fontWeight = FontWeight.Normal,
                color = colors.onBackground
            )
        )
    ) {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text("Links2Read") }
                )
            },
            floatingActionButton = {
                IconButton(
                    onClick = {
                        setDialogMode(Mode.ADD)
                        setLinkToEdit(null)
                        setIsDialogOpen(true)
                    },
                    modifier = Modifier
                        .size(56.dp)
                        .background(colors.secondary, CircleShape)
                ) {
                    Icon(
                        imageVector = Icons.Default.Add,
                        contentDescription = "Add Link",
                        tint = colors.onSecondary
                    )
                }
            }
        ) {
            Box(modifier = Modifier.fillMaxSize()) {
                val links = viewModel.links

                LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    contentPadding = PaddingValues(top = 56.dp + TopAppBarDefaults.windowInsets.asPaddingValues().calculateTopPadding(), start = 16.dp, end = 16.dp, bottom = 16.dp)
                ) {
                    items(links.size) { index ->
                        val link = links[index]
                        LinkItem(
                            link,
                            onEdit = {
                                setDialogMode(Mode.EDIT)
                                setIsDialogOpen(true)
                                setLinkToEdit(link)
                            },
                            onDelete = {
                                viewModel.deleteLink(link.id)
                            }
                        )
                    }
                }

                if (isDialogOpen) {

                    AnimatedVisibility(
                        visible = isDialogOpen,
                        enter = fadeIn() + expandIn(),
                        exit = fadeOut() + shrinkOut()
                    ) {
                        LinkDialog(
                            viewModel = viewModel,
                            mode = dialogMode,
                            link = linkToEdit,
                            onDismiss = { setIsDialogOpen(false) }
                        )
                    }
                }
            }
        }
    }
}