import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
    id("com.squareup.sqldelight") version "1.5.4"
}

sqldelight {
    database("Database") {
        packageName = "dev.santito.database"
    }
}

group = "dev.santito"
version = "1.0.0"

repositories {
    google()
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
}

dependencies {
    implementation(compose.desktop.currentOs)
    implementation(compose.material3)
    val sqlDeLight = "1.5.4"
    implementation("com.squareup.sqldelight:sqlite-driver:$sqlDeLight")
}

compose.desktop {
    application {
        mainClass = "dev.santito.AppKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "Links2Read"
            packageVersion = "1.0.0"
        }
    }
}
