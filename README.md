links2Read app with Desktop Compose UI.

Libraries used:
- Jetpack Compose - shared UI
- [SQLDelight](https://github.com/cashapp/sqldelight) - data storage

### Running desktop application
 * To run, launch command: `./gradlew :run`
